import "./styles.css"
import { useState, useEffect } from "react"

export default function CharCard({
	char: { id, name, status, image, species, location, episode },
}) {
	const [seen, setSeen] = useState("")

	useEffect(() => {
		fetch(episode[0])
			.then((resp) => resp.json())
			.then((resp) => setSeen(resp.name))
	}, [episode])

	return (
		<section className="char" key={id}>
			<img className="char__img" src={image} alt={name} />
			<div className="char__info">
				<h2 className="info__name">{name}</h2>
				<p className="info__status">
					<span
						className={`status__condition ${
							status === "Alive"
								? "status__condition--alive"
								: status === "Dead"
								? "status__condition--dead"
								: ""
						}`}
					></span>
					{status} - {species}
				</p>
				<div className="info__location">
					<p className="location__title">Last known location:</p>
					<a className="location__anchor" href={location.url}>
						{location.name}
					</a>
				</div>
				<div className="info__first-seen">
					<p className="first-seen__title">First seen in:</p>
					<a className="first-seen__anchor" href={episode[0]}>
						{seen}
					</a>
				</div>
			</div>
		</section>
	)
}
