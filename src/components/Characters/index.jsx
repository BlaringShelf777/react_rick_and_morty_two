import { useState, useEffect } from "react"
import CharCard from "../CharCard"
import "./styles.css"

export default function Characters() {
	const [next, setNext] = useState(null)
	const [prev, setPrev] = useState(null)
	const [curr, setCurr] = useState("https://rickandmortyapi.com/api/character")
	const [chars, setChars] = useState([])

	useEffect(() => {
		fetch(curr)
			.then((resp) => resp.json())
			.then(({ results, info: { next, prev } }) => {
				setNext(next)
				setPrev(prev)
				setChars(results)
			})
			.catch((err) => console.log(err))
	}, [curr])

	const handleNext = () => {
		if (next) setCurr(next)
	}

	const handlePrev = () => {
		if (prev) setCurr(prev)
	}

	return (
		<div>
			<div className="chars">
				{chars.map((char, index) => (
					<CharCard key={index} char={char} />
				))}
			</div>
			<button onClick={handlePrev}>Prev</button>
			<button onClick={handleNext}>Next</button>
		</div>
	)
}
