import "./App.css"
import Characters from "./components/Characters"

function App() {
	return (
		<div className="App">
			<div className="App-header">
				<Characters />
			</div>
		</div>
	)
}

export default App
